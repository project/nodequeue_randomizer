<?php // $Id$

/**
 * @file nodequeue_randomizer.admin.inc
 *
 * @author Greg Field
 * @since Nov 26th, 2009
 **/

/**
 * Admin settings for selecting which nodequeues are randomized and how often
 *
 * @param $form_state
 *   Array which represents the current form state
 *
 * @return
 *   Array which represents a Drupal form
 */
function nodequeue_randomizer_form_admin_settings(&$form_state) {
  // An unfortunate necessity to set a min date of the current time.  Form state doesn't persist
  // initially which prevents it from being used.
  if (!isset($_GET['time']) || !is_numeric($_GET['time'])) {
    // Use the current time but round down to the nearest minute
    drupal_goto('admin/content/nodequeue/randomizer', array('time' => time() - (time() % 60)));
  }

  global $user;
  $form = array();

  if (variable_get('configurable_timezones', 1) && $user->uid && strlen($user->timezone)) {
    $form['offset'] = array(
      '#type'  => 'value',
      '#value' => $user->timezone
    );
  }
  else {
    $form['offset'] = array(
      '#type'  => 'value',
      '#value' => variable_get('date_default_timezone', 0)
    );
  }

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('Randomizer Settings'), "admin/content/nodequeue/randomizer");
  drupal_set_breadcrumb($breadcrumb);

  $qids = nodequeue_get_all_qids(0);
  $queues = nodequeue_load_queues($qids);
  $subqueues = nodequeue_load_subqueues_by_queue($qids);
  $nodequeue_randomizer_queues = nodequeue_randomizer_get_queues(TRUE);

  usort($queues, '_nodequeue_randomizer_alphabetical_queue_sort');

  // Create the nodequeue fieldsets
  foreach ($queues as $queue) {
    $form["nodequeue_{$queue->qid}"] = array(
      '#type'        => 'fieldset',
      '#title'       => $queue->title,
      '#collapsible' => TRUE,
      '#collapsed'   => !isset($nodequeue_randomizer_queues[$queue->qid]),
      '#tree'        => TRUE
    );
  }

  // Create the subqueue fieldsets within the nodequeues
  foreach ($subqueues as $subqueue) {
    $randomizer_settings = (isset($nodequeue_randomizer_queues[$subqueue->qid][$subqueue->sqid])) ? $nodequeue_randomizer_queues[$subqueue->qid][$subqueue->sqid] : NULL;

    $subqueue_form = array(
      '#type'        => 'fieldset',
      '#title'       => $subqueue->title,
      '#tree'        => TRUE
    );

    $subqueue_form['is_randomized'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Randomize'),
      '#description'   => t('Turn randomization on and off for this subqueue.'),
      '#default_value' => (isset($randomizer_settings)) ? TRUE : FALSE,
      '#tree'          => TRUE
    );
    $subqueue_form['period'] = array(
      '#type'          => 'select',
      '#title'         => t('Randomization Period'),
      '#description'   => t('How often the contents of this subqueue will be randomized.'),
      '#options'       => array(
        3600    => t('1 hour'),
        21600   => t('6 hours'),
        43200   => t('12 hours'),
        86400   => t('1 day'),
        172800  => t('2 day'),
        604800  => t('1 week'),
        1209600 => t('2 week')
      ),
      '#default_value' => (isset($randomizer_settings)) ? $randomizer_settings['period'] : 86400,
      '#tree'          => TRUE
    );
    $subqueue_form['next_run'] = array(
      '#type'          => 'date_select',
      '#title'         => t('Next Randomization'),
      '#tree'          => TRUE
    );
    if (isset($randomizer_settings)) {
      $default_time = max($randomizer_settings['last_run'] + $randomizer_settings['period'], $_GET['time']) + $form['offset']['#value'];
      $subqueue_form['next_run']['#default_value'] = format_date($default_time, 'custom', 'Y-m-d H:i:s', 0);
    }
    else {
      $subqueue_form['next_run']['#default_value'] = format_date($_GET['time'] + $form['offset']['#value'], 'custom', 'Y-m-d H:i:s', 0);
    }

    $subqueue_form['view'] = array(
      '#type'  => 'markup',
      '#value' => format_plural($subqueue->count, '1 node in queue', '@count nodes in queue') . ' - ' . l(t('View this subqueue.'), "admin/content/nodequeue/{$subqueue->qid}/view/{$subqueue->sqid}")
    );

    $form["nodequeue_{$subqueue->qid}"]["subqueue_{$subqueue->sqid}"] = $subqueue_form;
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

/**
 * Validation for nodequeue randomizer admin settings
 *
 * @param $form
 *   Array which represents a Drupal form
 * @param $form_state
 *   Array which represents the current form state
 */
function nodequeue_randomizer_form_admin_settings_validate($form, &$form_state) {
  $values = $form_state['values'];
  $min_date = $_GET['time'];

  foreach ($values as $key => $value) {
    if (drupal_strlen($key) > 10 && drupal_substr($key, 0, 10) == 'nodequeue_') {
      foreach ($value as $subkey => $subvalue) {
        if (drupal_strlen($subkey) > 9 && drupal_substr($subkey, 0, 9) == 'subqueue_') {
          if ($subvalue['is_randomized']) {
            $next_run = strtotime($subvalue['next_run']) - $values['offset'];

            if ($next_run < $min_date) {
              $date = date(variable_get('date_format_short', 'm/d/Y - H:i'), $min_date + $values['offset']);
              form_set_error("{$value}][{$subvalue}][next_run", t('The specified time of the next run must on or after !date.', array('!date' => $date)));
            }
          }
        }
      }
    }
  }
}

/**
 * Submission for nodequeue randomizer admin settings
 *
 * @param $form
 *   Array which represents a Drupal form
 * @param $form_state
 *   Array which represents the current form state
 */
function nodequeue_randomizer_form_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];

  // Remove current entries in the randomizer table
  db_query("TRUNCATE TABLE {nodequeue_randomizer}");

  // Repopulate the table with the new values
  foreach ($values as $key => $value) {
    if (drupal_strlen($key) > 10 && drupal_substr($key, 0, 10) == 'nodequeue_') {
      $qid = substr($key, 10);

      foreach ($value as $subkey => $subvalue) {
        if (drupal_strlen($subkey) > 9 && drupal_substr($subkey, 0, 9) == 'subqueue_') {
          $sqid = substr($subkey, 9);

          if ($subvalue['is_randomized']) {
            $next_run = strtotime($subvalue['next_run']) - $values['offset'];
            $last_run = $next_run - $subvalue['period'];

            db_query(
              "INSERT
              INTO {nodequeue_randomizer}
              (qid, sqid, period, last_run)
              VALUES (%d, %d, %d, %d)",
              $qid, $sqid, $subvalue['period'], $last_run
            );
          }
        }
      }
    }
  }
}

/**
 * Form of setting the weighting of items within a subqueue for randomization
 *
 * @param $form_state
 *   Array which represents the current form state
 * @param $queue
 *   The nodequeue object who's subqueue is being edited
 * @param $subqueue (optional)
 *   The subqueue of the nodequeue who's nodes are being weighted, optional if nodequeue has only 1 subqueue
 *
 * @return
 *   Array which represents a Drupal form
 */
function nodequeue_randomizer_form_admin_weight(&$form_state, $queue, $subqueue = NULL) {
  $form = array();

  if (!isset($subqueue) || !$subqueue->sqid) {
    if ($queue->subqueues == 1) {
      $subqueues = nodequeue_load_subqueues_by_queue($queue->qid);
      $subqueue = array_shift($subqueues);
    }
    else {
      // NOTE: This is not tested as our site does not have a nodequeue with mutiple subqueues
      $subqueue_list = nodequeue_view_subqueues($queue);
      $subqueue_list = preg_replace('/admin\/content\/nodequeue\/(\d+)\/view\/(\d+)/', 'admin/content/nodequeue/$1/weight/$2', $subqueue_list);

      return array(
        'subqueue_list' => array(
          '#type' => 'markup',
          '#value' => $subqueue_list
        )
      );
    }
  }

  if (!nodequeue_api_subqueue_access($subqueue, NULL, $queue)) {
    return drupal_not_found();
  }

  drupal_set_title(t("Subqueue '@title'", array('@title' => nodequeue_title_substitute($queue->subqueue_title, $queue, $subqueue))));
  $form['_queue'] = array(
    '#type'  => 'value',
    '#value' => $queue
  );
  $form['_subqueue'] = array(
    '#type'  => 'value',
    '#value' => $subqueue
  );

  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l($queue->title, "admin/content/nodequeue/$queue->qid");
  drupal_set_breadcrumb($breadcrumb);

  $results = db_query(
    "SELECT n.nid, n.title, nn.weight
    FROM {node} n
    INNER JOIN {nodequeue_nodes} nn ON (n.nid = nn.nid)
    WHERE nn.sqid = %d
    ORDER BY nn.position ASC",
    $subqueue->sqid
  );

  while ($node_info = db_fetch_object($results)) {
    $form["node_{$node_info->nid}"] = array(
      '#type'          => 'textfield',
      '#default_value' => $node_info->weight,
      '#maxlength'     => 4,
      '#size'          => 6,
      '#attributes'    => array(
        'title' => $node_info->title
      )
    );
  }

  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

/**
 * Submission for setting the weighting of items within a subqueue for randomization
 *
 * @param $form
 *   Array which represents a Drupal form
 * @param $form_state
 *   Array which represents the current form state
 */
function nodequeue_randomizer_form_admin_weight_submit($form, &$form_state) {
  $values = $form_state['values'];

  foreach ($values as $key => $value) {
    if (strlen($key) > 5 && substr($key, 0, 5) == 'node_') {
      $nid = substr($key, 5);

      db_query(
        "UPDATE {nodequeue_nodes}
        SET weight = %d
        WHERE qid = %d AND sqid = %d AND nid = %d",
        $value, $values['_queue']->qid, $values['_subqueue']->sqid, $nid
      );
    }
  }
}

/**
 * Function used by usort to sort nodequeues in alphabetical order
 *
 * @param $a
 *   Nodequeue object used in ordering comparison
 * @param $b
 *   Nodequeue object used in ordering comparison
 *   
 * @return
 *   Integer value denoting the result of comparison, either -1, 0, or 1
 */
function _nodequeue_randomizer_alphabetical_queue_sort($a, $b) {
  $title_a = strtolower($a->title);
  $title_b = strtolower($b->title);

  if ($title_a == $title_b) {
    return 0;
  }

  return ($title_a < $title_b) ? -1 : 1;
}
